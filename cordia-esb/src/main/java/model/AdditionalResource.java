package model;

import io.swagger.annotations.ApiModelProperty;

public class AdditionalResource {

	@ApiModelProperty(value = "value", allowableValues = "value", example = "value")
	public String resourceName;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "OtherResource")
	public String resourceType;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0010Y00000bNvossA1")
	public String resourceId;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0010Y00000bZ4ssee2")
	public String collectiveId;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "Wheelers space")
	public String productType;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "Sold")
	public String resourceStatus;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "Finished")
	public String constructionStatus;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0010Y0000zduHASSW5")
	public String investmentId;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0010Y0000zduHASSW6")
	public String stageId;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0010Y0000zduHASSW7")
	public String buildingId;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "OtherResource")
	public String resourceNumber;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "OtherResource")
	public String resourceFinalNumber;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "value")
	public String bankAccountNumber;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "ING")
	public String bankName;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "0")
	public Double floorLevel;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "12.2")
	public Double price;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "10.0")
	public Double netPrice;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "2.0")
	public Double pricePerSqm;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "9.0")
	public Double minimumPrice;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "8.6")
	public Double budgetPrice;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "PLN")
	public String currencyIsoCode;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "9.1")
	public Double priceWithDiscount;
	
	@ApiModelProperty(value = "value", allowableValues = "value", example = "9.0")
	public Double netPriceWithDiscount;

}
