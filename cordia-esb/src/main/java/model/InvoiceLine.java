package model;

import java.math.BigDecimal;

import javax.validation.constraints.Max;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class InvoiceLine {

	@ApiModelProperty(value = "Description", allowableValues = "Sample desc", example="Sample desc", required = false)
    public String description;

	@ApiModelProperty(value = "Invoice line gross revenue", example = "1000.10", required = true)
    public BigDecimal amount;

	@ApiModelProperty(value = "Vat value", example = "230.0", required = true)
    public BigDecimal vatAmount;

	@Max(10)
	@ApiModelProperty(value ="Value from AX table", allowableValues = "CUS-EU, CUS-HUN, CUS-NONEU, CUS-POL,CUS-RO", example="CUS-HUN")
    public String salesTaxGroup;

	@Max(20)
	@ApiModelProperty(value = "Value from AX table", allowableValues="PRODG-0, PRODG-FULL, PRODG-MIN, PRODG-RED", example="PRODG-FULL")
    public String itemSalesTaxGroup;

	@ApiModelProperty(value = "Proper reason code reference based on reason code table", 
			allowableValues = "CASH09, CASH10,CASH11,CASH12,CASH13,CASH14,REV01,REV02,REV03,REV04,REV05,REV06,REV07,REV08,REV09,REV10,REV11,REV12,REV13,REV14,REV15,REV16,REV17,REV18,REV19",
			example = "CASH09")
    public String reasonCode;

	@ApiModelProperty(value = "Agreement dimension - Salesforce agreement Id", example = "a0t0Y000002sLtCQAU")
    public String finDim_Agreement;

	@ApiModelProperty(value = "Can be determined from property", example = "1_4_05")
    public String cFCode;

	@ApiModelProperty(value = "Development project Salesforce Id", example = "a0t0J000002sLtTFGS" )
    public String developmentProject;

	@ApiModelProperty(value = "Property Id", example = "a0t0J000004sLtYDUW")
    public String property;

	@ApiModelProperty(value = "SF Invoice Id", allowableValues = "a0T0Y000000wY0MUAU", example="a0T0Y000000wY0MUAU")
    public String invoiceId;

	@Override
	public String toString() {
		return "InvoiceLine [description=" + description + ", amount=" + amount + ", vatAmount=" + vatAmount
				+ ", salesTaxGroup=" + salesTaxGroup + ", itemSalesTaxGroup=" + itemSalesTaxGroup + ", reasonCode="
				+ reasonCode + ", finDim_Agreement=" + finDim_Agreement + ", cFCode=" + cFCode + ", developmentProject="
				+ developmentProject + ", property=" + property + ", invoiceId=" + invoiceId + "]";
	}


	
	
}
