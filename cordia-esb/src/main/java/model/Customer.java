package model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.sf.saxon.functions.True;

@ApiModel(value = "Customer")
@Data
@JsonAutoDetect
public class Customer {

	@ApiModelProperty(value = "Determined by SF", allowableValues = "1,2", example = "1", required = true)
	public Integer dirPartyType;

	@ApiModelProperty(value = "Concatenated FirstName + LastName", allowableValues = "John Tester", example = "John Tester")
	public String name;

	@ApiModelProperty(value = "Customer account id from SF", allowableValues = "0010Y00001FJZWeQAP", example = "0010Y00001FJZWeQAP", required = true)
	public String salesforceCustomerId;

	@ApiModelProperty(value = "Defines the language of the customer", allowableValues = "hu, pl, en, en-za", example = "hu", required = true)
	public String languageId;

	@ApiModelProperty(value = "Street name and number", allowableValues = "Dream Street 7", example = "Dream Street 7")
	public String street;

	@ApiModelProperty(value = "Zip Code", allowableValues = "1034", example="1034", required = true)
	public String zipCode;

	@ApiModelProperty(value = "Country code ISO-3166", allowableValues = "HUN, POL", example = "HUN", required = true)
	public String countryRegionId;

	@ApiModelProperty(value = "Customer City", allowableValues = "Dream City", example="Budapest")
	public String city;

	@ApiModelProperty(value = "Classification of the customer", allowableValues = "CUS-HUN", example="CUS-HUN", required = true)
	public String customerGroup;

	@ApiModelProperty(value = "Currency in what the customer is transacting in", allowableValues = "EUR, PLN", required = true)
	public String defaultCurrency;

	@ApiModelProperty(value = "VAT classification of the customer - drives VAT calculation and posting", allowableValues = "CUS-EU,CUS-HUN,CUS-NONEU,CUS-POL,CUS-RO", example = "CUS-HUN", required = true)
	public String taxGroup;

	@ApiModelProperty(value = "checked formats ( NIP, PESEL, PESEL/PESEL..)", allowableValues = "8238493712", example="74041367321", required = true)
	public String vatNum;

	@ApiModelProperty(value = "It is client personal bank account", allowableValues = "2345723495837485934834534", example="2345723495837485934834534")
	public String customerBankAccount;

	@ApiModelProperty(value = "It is client escrow bank account", allowableValues = "2345723495837485934834535", example="2345723495837485934834535")
	public String customerEscrowBankAccount;

	@ApiModelProperty(value = "It is client escrow bank Group Id", allowableValues = "CITI", example="CITI")
	public String customerEscrowBankGroup;

	@Override
	public String toString() {
		return "Customer [dirPartyType=" + dirPartyType + ", name=" + name + ", salesforceCustomerId="
				+ salesforceCustomerId + ", languageId=" + languageId + ", street=" + street + ", zipCode=" + zipCode
				+ ", countryRegionId=" + countryRegionId + ", city=" + city + ", customerGroup=" + customerGroup
				+ ", defaultCurrency=" + defaultCurrency + ", taxGroup=" + taxGroup + ", vatNum=" + vatNum
				+ ", customerBankAccount=" + customerBankAccount + ", customerEscrowBankAccount="
				+ customerEscrowBankAccount + ", customerEscrowBankGroup=" + customerEscrowBankGroup + "]";
	}

}
