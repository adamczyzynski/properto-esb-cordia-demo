package httptoftp;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import support.PropertiesHelper;

public class FileDeleteHelper implements Callable {

	public static PropertiesHelper propertiesHelper = new PropertiesHelper();
	final String FTP_ADDRESS = propertiesHelper.getFtpAddress();
	final String PORT = propertiesHelper.getFtpPort();
	final String USER = propertiesHelper.getFtpLogin();
	final String PASSWORD = propertiesHelper.getFtpPassword();
	final String ADDRESS = propertiesHelper.getFtpAddress();

	Properties prop;
	InputStream input = null;

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		String filedel = (String) eventContext.getMessage().getProperty(
				"contentVar", PropertyScope.SESSION);
		eventContext.getMessage().setProperty("delFile",
				filedel, PropertyScope.INVOCATION);

	return eventContext.getMessage().getPayload();
		
	}

}
