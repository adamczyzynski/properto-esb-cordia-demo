package restServices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import axPoster.AxHttpSender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import model.AdditionalResource;
import model.Agreement;
import support.EmailSender;

@Api(value = "/AdditionalResource")
@Consumes("application/json")
@Produces("application/json")
@Path("/additionalResource")
@PropertySource("application.properties")
public class AdditionalResourceService {

	private static Logger logger = LoggerFactory.getLogger(AgreementService.class);
	private static final String axPath = "/api/WorkOrderSheet";

	@POST
	@ApiOperation(value = "New AdditionalResource from Salesforce", authorizations = { @Authorization(value = "basicAuth") })
	public Response addAgreement(
			@ApiParam(value = "new AdditionalResource", required = true) AdditionalResource additionalResource) {

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(additionalResource);
		} catch (JsonProcessingException e) {
			logger.error("Problem with jsonParser: " + e.getStackTrace() + "agreement: " + additionalResource);
			new EmailSender("AdditionalResource", "JSON parse error: " + e.getStackTrace() + additionalResource.toString())
					.sendErrorEmail();
			e.printStackTrace();
		}
		logger.info("Created new agreement: " + additionalResource);

		AxHttpSender axHttpSender = new AxHttpSender(axPath, "NONE");
		axHttpSender.setHeader("content-type", "application/json; charset=utf-8");
		axHttpSender.addBody(jsonInString);
		HttpResponse httpResponse = axHttpSender.httpPost();
		logger.debug("Create new AdditionalResource: " + additionalResource);

		if (httpResponse == null) {
			logger.debug("Connection Problem: " + "agreement: cannot send: " + additionalResource);
			new EmailSender("AdditionalResource", "Connection problem: " + additionalResource.toString()).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		logger.info("Response: " + httpResponse.getStatusLine().getStatusCode());
		new EmailSender("AdditionalResource", "Success: " + additionalResource.toString()).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}

}
