package restServices;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import axPoster.AxHttpSender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import model.Customer;
import support.EmailSender;

@Api(value = "/customers")
@Produces("application/json")
@Consumes("application/json")
@Path("/customers")
public class CustomerService {
	private static Logger logger = LoggerFactory.getLogger(CustomerService.class);
	private static final String axPath = "/api/Customer";

	@POST
	@ApiOperation(httpMethod = "POST" , value = "New Customer from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "New Customer from Salefsorce updated"),
			@ApiResponse(code = 500, message = "Connection problem with AX") })
	public Response addCustomer(@ApiParam(value = "new Customer", required = true) Customer customer) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		logger.info("Test new customer: "+customer.toString());
		try {
			jsonInString = mapper.writeValueAsString(customer);
		} catch (JsonProcessingException e) {
			logger.error("TestProblem with jsonParser: " + e.getStackTrace() + "customer: " + customer);
			new EmailSender("New customer", "Problem with jsonParser: " + e.getMessage() + "customer: " + customer.toString()).sendErrorEmail();
			e.printStackTrace();
		}


		AxHttpSender axHttpSender = new AxHttpSender(axPath, "NONE");
		axHttpSender.setHeader("content-type", "application/json; charset=utf-8");
		axHttpSender.addBody(jsonInString);
		HttpResponse httpResponse = axHttpSender.httpPost();

		logger.debug("Create new customer: "+ customer);
		if (httpResponse == null) {
			logger.debug("Connection Problem: " + "customer cannot send: " + customer);
			new EmailSender("New customer", "Connection problem: "+customer.toString()).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		new EmailSender("New customer", "Success json: "+jsonInString).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}

	@PUT
	@ApiOperation(httpMethod = "PUT", value = "New Customer from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer from Salefsorce updated"),
			@ApiResponse(code = 500, message = "Connection problem with AX") })
	@Produces("application/json")
	public Response updateCustomer(@ApiParam(value = "new Customer", required = true) Customer customer) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		logger.info("Test: "+customer.toString());
		try {
			jsonInString = mapper.writeValueAsString(customer);
		} catch (JsonProcessingException e) {
			logger.error("TestProblem with jsonParser: " + e.getStackTrace() + "customer: " + customer);
			new EmailSender("New customer", "Problem with jsonParser: " + e.getMessage() + "customer: " + customer.toString()).sendErrorEmail();
			e.printStackTrace();
		}

		logger.info("Test Created new customer: " + customer);
		logger.info("Customer as json: "+jsonInString);

		AxHttpSender axHttpSender = new AxHttpSender(axPath, customer.countryRegionId);
		axHttpSender.setHeader("content-type", "application/json;charset=UTF-8");

		axHttpSender.addBody(jsonInString);
		HttpResponse httpResponse = axHttpSender.httpPut();
		if (httpResponse == null) {
			logger.debug("Connection Problem: " + "customer cannot send: " + customer);
			new EmailSender("New customer", "Connection problem: "+customer.toString()).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		new EmailSender("New customer", "Success: "+customer.toString()).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}

}
