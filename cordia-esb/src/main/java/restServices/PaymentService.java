package restServices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import model.Payment;

@Api(value = "/Payment")
@Consumes("application/json")
@Produces("application/json")
@Path("/payment")
public class PaymentService {

	
	@POST
	@ApiOperation(value = "New payment from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@Produces("application/json")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "ok")})
	public static Response addPayment(@ApiParam(value = "Payment", required = true) Payment payment) {

		System.out.print("New payment: "+ payment);
		return Response.ok(payment.toString()).build();
	}
	
}
