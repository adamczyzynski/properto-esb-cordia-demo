package support;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import restServices.CustomerService;

public class EmailSender {

	private static Logger logger = LoggerFactory.getLogger(EmailSender.class);
	
	private final PropertiesHelper propertiesHelper = new PropertiesHelper();

	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	private String message;

	private String title;

	public EmailSender(String title, String message) {
		this.title = title;
		this.message = message;
	}

	public void sendInformationEmail() {
		sendEmail("INFO");
	}

	public void sendErrorEmail() {
		sendEmail("ERROR");
	}

	private void sendEmail(String type) {
		logger.info("New email");
		if (propertiesHelper.getEmailsEnable()) {
			Properties props = new Properties();

			LocalDateTime now = LocalDateTime.now();

			props.put("mail.smtp.host", propertiesHelper.getSmtpHost());
			props.put("mail.smtp.socketFactory.port", propertiesHelper.getSmtpSocketFactoryPort());
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", propertiesHelper.getSmtpPort());

			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(propertiesHelper.getSmtpAddress(),
							propertiesHelper.getSmtpPassword());
				}
			});

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(propertiesHelper.getSmtpAddress()));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(propertiesHelper.getOutEmailAddress()));
				message.setSubject(type + ": " + this.title);
				message.setText(String.valueOf(dtf.format(now)) + ": " + this.message);

				Transport.send(message);
				System.out.println("Done");

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}
	}

}
