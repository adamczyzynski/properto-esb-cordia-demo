package axPoster;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;

import javax.resource.spi.IllegalStateException;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.CoreProtocolPNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import support.EmailSender;
public class AxHttpSender {
	private static final Logger logger = LoggerFactory.getLogger(AxHttpSender.class);

	private static final String hostUrlHun2009Ax = "http://185.161.73.76:8186";

	private String path;
	private HttpClient httpClient = HttpClientBuilder.create().build();
	private StringEntity params;
	private HttpPost httpPost;
	private HttpPut httpPut;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public static String getHosturl() {
		return hostUrlHun2009Ax;
	}

	public StringEntity getParams() {
		return params;
	}

	public AxHttpSender(String path, String isoCode) {
		this.path = path;
		this.httpPost = new HttpPost(hostUrlHun2009Ax + path);
		this.httpPut = new HttpPut(hostUrlHun2009Ax + path);
		

	}

	public void addBody(String body) {
		logger.info("body before: "+body);
		this.params = new StringEntity(body, Consts.UTF_8);

		System.out.println("body after: "+this.params);

		this.httpPost.setEntity(params);
		this.httpPost.setHeader(CoreProtocolPNames.HTTP_CONTENT_CHARSET, Consts.UTF_8.toString());
	}

	public void setHeader(String key, String value) {
		this.httpPost.addHeader(key, value);
		this.httpPut.addHeader(key, value);
	}

	public HttpResponse httpPost() {
		logger.info("New Post to: " + this.path);
		HttpResponse httpResponse = null;
		try {
			logger.info("body: "+ this.params);
			httpResponse= this.httpClient.execute(this.httpPost);
		} catch (ClientProtocolException e) {
			logger.error("Ax Http sender POST", "Client Protocol Exception: "+e.getStackTrace());
			e.printStackTrace();
			new EmailSender("Ax Http sender POST", "Client Protocol Exception: "+e.getStackTrace()).sendErrorEmail();
		
		} catch (ConnectException connectException) {
			logger.error("fail post to: " + this.path + " " + connectException.getStackTrace());
			new EmailSender("Ax Http sender POST", "connect exception: "+connectException.getStackTrace()).sendErrorEmail();

			connectException.printStackTrace();
		} catch (IOException e) {
			new EmailSender("Ax Http sender, POST", "IO exception: "+e.getStackTrace()).sendErrorEmail();
			logger.error("AX Http sender IO exception POST: "+e.getStackTrace());
			e.printStackTrace();
		} 
		return httpResponse;
	}

	public HttpResponse httpPut() {
		HttpResponse httpResponse = null;

		try {
			httpResponse = this.httpClient.execute(this.httpPut);
		} catch (ClientProtocolException e) {
			new EmailSender("Ax Http sender PUT", "Client Protocol Exception: "+e.getStackTrace()).sendErrorEmail();;

			e.printStackTrace();
		} catch (IOException e) {
			logger.debug("AX Http sender IO exception PUT: "+e.getStackTrace());
			e.printStackTrace();
		}
		return httpResponse;
	}
	

}
